import React, { Component } from 'react';
import {Header} from './Header/header.js'
import {Body} from './Body/body.js'
import 'semantic-ui-css/semantic.min.css';
import './App.css';

class App extends Component {
  render() {
    return (
    	<div>
	      <div>
	      	<Header />
	      </div>
	      <div>
	      	<Body />
	      </div>
    	</div>
    );
  }
}

export default App;
