import React from 'react'
import { Divider, Input, Button, Form } from 'semantic-ui-react'
import './login.css'

export class Login extends React.Component{
	constructor(){
		super()
		this.userChange = this.userChange.bind(this)
		this.passwordChange = this.passwordChange.bind(this)
		this.loginSubmit = this.loginSubmit.bind(this)

		this.username=null
		this.password=null
	}

	userChange(e, data){
		e.persist()
		this.username=data.value
	}

	passwordChange(e, data){
		e.persist()
		this.password=data.value
	}

	loginSubmit(e, data){
		e.preventDefault()
		this.props.callBack(this.username, this.password)
	}

	render(){
		return(
			<div>
				<Form onSubmit={this.loginSubmit}>
					<Divider horizontal>Login</Divider>
					<div className="user-div">
						<h3>User:</h3>
						<Input className="login-user-input" fluid onChange={this.userChange} placeholder="Enter username..." />
					</div>
					<div className="password-div">
						<h3>Password:</h3>
						<Input className="login-password-input" fluid onChange={this.passwordChange} placeholder="Enter password..." />
					</div>
					<Button type='submit' primary className="login-submit-button">Submit</Button>
				</Form>
			</div>
			);
	}
}