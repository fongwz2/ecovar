import React from 'react'
import { Divider, Segment, Message, Button } from 'semantic-ui-react'
import axios from 'axios'

export class Confirmation extends React.Component{
	constructor(){
		super()
		this.state={details: null}

		this.onSubmit = this.onSubmit.bind(this)
		this.onBack = this.onBack.bind(this)
	}

	static getDerivedStateFromProps(nextProps, prevState){
		return {
			details: nextProps
		}
	}

	onSubmit(e, data){
		e.preventDefault()
		axios.post( 'http://ecovar.snapmeter.xyz/email', {
			data: {
				cusname: this.state.details.details.cusname,
				country: this.state.details.details.country,
				email: this.state.details.details.mail1 + "@" + this.state.details.details.mail2,
				comments: this.state.details.details.comments,
				name: this.state.details.details.name,
				product: this.state.details.details.product,
				purity: this.state.details.details.purity,
				period: this.state.details.details.leadTime,
				supplier: this.state.details.details.supplier,
				capex: this.state.details.details.capex,
			}
		}).then(res => {
			if(res.status === 200){
				this.props.callBack()
			}
		})
		//this.props.callBack()
	}

	onBack(e, data){
		e.preventDefault()
		this.props.callBack()
	}

	render(){
		return(
			<div>
				<Divider horizontal={true}>Confirm Details</Divider>
				<Message>Please double check customer and product details.</Message>
				<Segment className="customer-segment">
					<p><b>Name: {this.state.details.details.cusname}</b></p>
					<p><b>Country: {this.state.details.details.country}</b></p>
					<p><b>Email: {this.state.details.details.mail1}@{this.state.details.details.mail2}</b></p>
					<p><b>Additional Comments: {this.state.details.details.comments}</b></p>
					<p><b>Product Name: {this.state.details.details.name}</b></p>
					<p><b>Product: {this.state.details.details.product}</b></p>
					<p><b>Purity: {this.state.details.details.purity}</b></p>
					<p><b>Lead Time: {this.state.details.details.leadTime} Months</b></p>
					<p><b>Supplier: {this.state.details.details.supplier}</b></p>
				</Segment>
				<Button size="huge" className="confirmation-submit-button" basic color="green" onClick={this.onSubmit} toggle={false}>Confirm Details</Button>
				<Button size="huge" className="confirmation-back-button" basic color="red" onClick={this.onBack} toggle={false}>Back</Button>
			</div>
			);
	}
}