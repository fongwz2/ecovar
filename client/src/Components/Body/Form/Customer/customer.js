import React from 'react'
import { Segment, Input, TextArea} from 'semantic-ui-react'
import './customer.css'

export class Customer extends React.Component{
	constructor(){
		super()
		this.handleChange = this.handleChange.bind(this)
	}

	handleChange(e, data){
		e.persist()
		this.props.callBack(data, e.target.value)
	}

	render(){
		return(
			<Segment raised className="customer-segment">
				<div>
					<b>Name:</b>
					<Input fluid className="customer-input-name" onChange={e=>this.handleChange(e,"name")} placeholder="Enter Customer's Name..." />
				</div>
				<div>
					<b>Country:</b>
					<Input fluid className="customer-input-country" onChange={e=>this.handleChange(e,"country")} placeholder="Enter Customer's Country..." />
				</div>
				<div>
					<b>Email:</b>
					<Input className="customer-input-email" onChange={e=>this.handleChange(e,"email1")} placeholder="" />
					<Input label='@' labelPosition='left' className="customer-input-email2" onChange={e=>this.handleChange(e,"email2")} placeholder=".com" />
				</div>
				<div>
					<b>Additional Information:</b>
					<TextArea rows={4} autoheight="true" onChange={e=>this.handleChange(e,"comments")} placeholder="Enter comments here..."/>
				</div>
			</Segment>
			);
	}
}