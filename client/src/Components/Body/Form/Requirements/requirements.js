import React from 'react'
import { Segment, Input, Dropdown } from 'semantic-ui-react'
import './requirements.css'

export class Requirements extends React.Component{
	constructor(){
		super()
		this.handleChange = this.handleChange.bind(this)
		this.productOptions=[
		{
			text: 'N2',
			value: 'n2'
		},{
			text: 'O2',
			value: 'o2'
		},{
			text: 'H2',
			value: 'h2'
		}]
	}

	handleChange(e, data){
		e.persist()
		if(data==="purity" || data==="period"){
			this.props.callBack(data, e.target.value)
		} else {
			this.props.callBack("product", data.value)
		}
	}

	render(){
		return(
			<Segment raised className="customer-segment">
				<div className="div-product">
					<b>Product:</b><Dropdown className="requirements-input-product" name="blah" onChange={this.handleChange} placeholder="Select Product..." options={this.productOptions}/>
				</div>
				<div className="div-purity">
					<b>Purity:</b><Input className="requirements-input-purity" type="number" step="any" onChange={e=>this.handleChange(e,"purity")} placeholder="Choose from 0-100%" />
				</div>
				<div>
					<b>Time Period:</b><Input className="requirements-input-time" type="number" onChange={e=>this.handleChange(e,"period")} placeholder="Enter from 1-12"/>
				</div>
			</Segment>
			);
	}
}