import React from 'react'
import axios from 'axios'
import {Customer} from './Customer/customer.js'
import {Requirements} from './Requirements/requirements.js'
import {Products} from './Rec_Products/products.js'
import { Divider, Form, Button, Message } from 'semantic-ui-react'
import './form.css'

export class Forms extends React.Component{
	constructor(){
		super()
		this.state={loading: false, payload: null}

		this.formSubmit = this.formSubmit.bind(this)
		this.customerCallBack = this.customerCallBack.bind(this)
		this.requirementsCallBack = this.requirementsCallBack.bind(this)
		this.productsResetCallBack = this.productsResetCallBack.bind(this)
		this.productsSubmitCallBack = this.productsSubmitCallBack.bind(this)

		this.name=null
		this.country=null
		this.mail1=null
		this.mail2=null
		this.comments=null
		this.product=null
		this.purity=null
		this.period=null
	}

	formSubmit(e, data){
		e.preventDefault()
		this.setState({loading: true})
		axios.post( 'http://ecovar.snapmeter.xyz/test', {
			data: {
				product: this.product,
				purity: this.purity,
				period: this.period
			}
		}).then(res => {
			this.setState({payload: res.data})
		})
		this.setState({loading: false})
	}

	customerCallBack(type, data){
		if(type==="name"){ this.name=data }
		else if(type==="country"){ this.country=data }
		else if(type==="email1"){ this.mail1 = data }
		else if(type==="email2"){ this.mail2 = data }
		else if(type==="comments"){ this.comments = data }
	}

	requirementsCallBack(type, data){
		if(type==="product"){ this.product=data }
		else if(type==="purity"){ this.purity=data }
		else if(type==="period"){ this.period=data }
	}

	productsResetCallBack(){
		this.setState({payload: null})
	}

	productsSubmitCallBack(details){
		details.cusname=this.name
		details.country=this.country
		details.mail1=this.mail1
		details.mail2=this.mail2
		details.comments=this.comments
		details.product=this.product
		this.props.callBack(details)
	}

	render(){
		let view;
		view = (
			this.state.payload === null?
			<Button size="huge" className='requirements-button' basic color='green' type='submit'>Submit</Button>
			:!this.name||!this.country||!this.mail1||!this.mail2||!this.product||!this.purity||!this.period?
			<div>
				<Message className='requirements-warning-message' color='red'>
					<Message.Header>You have one or more empty fields!</Message.Header>
					<p>Please fill in the form.</p>
				</Message>
				<Button size="huge" className='requirements-button' basic color='green' type='submit'>Submit</Button>
			</div>
			:this.state.payload.length<=0?
			<div>
				<Message className='requirements-warning-message' color='red'>
					<Message.Header>No products meet your requirements!</Message.Header>
					<p>Please try again with different requirements</p>
				</Message>
				<Button size="huge" className='requirements-button' basic color='green' type='submit'>Submit</Button>
			</div>
			:
			<div>
				<Divider horizontal>Recommended Products</Divider>
				<Products payload={this.state.payload} resetCallBack={this.productsResetCallBack} submitCallBack={this.productsSubmitCallBack}/>
			</div>
			);

		return(
			<Form onSubmit={this.formSubmit} loading={this.state.loading}>
				<Divider horizontal>Customer Information</Divider>
				<Customer callBack={this.customerCallBack}/>
				<Divider horizontal>Requirements</Divider>
				<Requirements callBack={this.requirementsCallBack}/>
				{view}
			</Form>
			);
	}
}