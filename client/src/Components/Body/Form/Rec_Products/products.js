import React from 'react'
import { Segment, List, Divider, Button, Message } from 'semantic-ui-react'

export class Products extends React.Component{
	constructor(){
		super()
		this.state={payload:null, box0: false, box1: false, box2: false, selectWarning: false}
		this.handleCheckboxClick = this.handleCheckboxClick.bind(this)
		this.onReset = this.onReset.bind(this)
		this.onSubmit = this.onSubmit.bind(this)
		this.listItems = []
	}

	static getDerivedStateFromProps(nextProps, prevState){
		return {
			payload: nextProps.payload
		}
	}

	handleCheckboxClick(event, data){
		event.persist()
		if(data.id==="box0"){ this.setState({box0: !this.state.box0, box1: false, box2: false})}
		else if(data.id==="box1"){ this.setState({box0: false, box1: !this.state.box1, box2: false})}
		else if(data.id==="box2"){ this.setState({box0: false, box1: false, box2: !this.state.box2})}			
	}

	onSubmit(event, data){
		event.preventDefault()
		if(!this.state.box0 && !this.state.box1 && !this.state.box2) {
			this.setState({selectWarning: true})
		} else {
			this.setState({selectWarning:false})
			this.props.submitCallBack(this.state.box0?this.state.payload[0]
				:this.state.box1?this.state.payload[1]
				:this.state.payload[2])
		}
	}

	onReset(event, data){
		event.preventDefault()
		this.setState({payload: null, selectWarning: false})
		this.props.resetCallBack()
	}

	render(){
		this.listItems.splice(0, this.listItems.length)
		for(var i = 0; i < this.state.payload.length; i++){
			if(i>=3){ break; }
			this.listItems.push(
				<div key={i}>
					<List horizontal={true} >
						<List.Item>
							<List.Content>
								<List.Header>
								{this.state.payload[i].supplier}
								</List.Header>
								Supplier
							</List.Content>
						</List.Item>

						<List.Item>
							<List.Content>
								<List.Header>
								{this.state.payload[i].name}
								</List.Header>
								Name
							</List.Content>
						</List.Item>

						<List.Item>
							<List.Content>
								<List.Header>
								{this.state.payload[i].purity}%
								</List.Header>
								Purity
							</List.Content>
						</List.Item>

						<List.Item>
							<List.Content>
								<List.Header>
								{this.state.payload[i].leadTime} Months
								</List.Header>
								Lead Time
							</List.Content>
						</List.Item>

						<List.Item>
							<List.Content>
								<List.Header>
								${this.state.payload[i].capex}
								</List.Header>
								CapEx
							</List.Content>
						</List.Item>

						<List.Item>
							<List.Content>
								<List.Header>
								${Math.floor(this.state.payload[i].capex/this.state.payload[i].leadTime)}
								</List.Header>
								Monthly Fee
							</List.Content>
						</List.Item>
					</List>

					<List floated="right" horizontal={true}>
						<List.Item>
							<Button 
								id={"box" + i} 
								onClick={this.handleCheckboxClick} 
								toggle={true}
								active={i===0?this.state.box0:i===1?this.state.box1:this.state.box2}>
								Select this Product
							</Button>
						</List.Item>
					</List>

					{/* create spacing */}
					<List horizontal={false}>
					</List>
					{
						i!==2 && i!==this.listItems.length-1 &&
						<Divider />
					}
				</div>
				)
			//console.log("assigning checkbox" + i + " to be " + (i===0?this.state.checkbox0:i===1?this.state.checkbox1:i===2?this.state.checkbox2:false))
		}
		return(
			<div>
				<Segment raised className="customer-segment">
					{this.listItems}	
				</Segment>

				<Segment basic className="product-segment">
				{
					this.state.selectWarning &&
					<Message className='requirements-warning-message' color='red'>
						<Message.Header>You have not selected any products!</Message.Header>
						<p>Please choose one before submitting for approval.</p>
					</Message>
				}
				<Button size="huge" className="product-submit-button" basic color="green" onClick={this.onSubmit} toggle={false}>Submit for OPD Approval</Button>
				<Button size="huge" className="product-reset-button" basic color="red" onClick={this.onReset} toggle={false}>Reset</Button>
				</Segment>
			</div>
			);

	}
}