import React from 'react'
import {Forms} from './Form/form.js'
import {Login} from './Login/login.js'
import {Confirmation} from './Confirmation/confirmation.js'

export class Body extends React.Component{
	constructor(){
		super()
		this.state = {login: false, confirm: false, confirmDetails: null, user: null, pw: null}
		this.loginCallBack = this.loginCallBack.bind(this)
		this.formCallBack = this.formCallBack.bind(this)
		this.confirmationCallBack = this.confirmationCallBack.bind(this)
	}

	loginCallBack(user, pw){
		/* hardcode values for testing phase */
		if(user==="test" && pw==="test"){
			this.setState({
				login: true,
				user: user,
				pw: pw
			})
		}
	}

	formCallBack(details){
		this.setState({confirm: true, confirmDetails: details})
	}

	confirmationCallBack(){
		this.setState({confirm: false, confirmDetails: null})
	}

	render(){
		let view;
		if(this.state.login && this.state.confirm){
			view=(<Confirmation details={this.state.confirmDetails} callBack={this.confirmationCallBack}/>);
		} else if(this.state.login){
			view=(<Forms callBack={this.formCallBack}/>);
		} else {
			view=(<Login callBack={this.loginCallBack}/>);
		}
		return(
			<div>
				{view}
			</div>
			);
	}
}