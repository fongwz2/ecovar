const express = require('express')
const Excel = require('exceljs');
const cors = require('cors');
const bodyParser = require('body-parser');
const PDFDocument = require('pdfkit')
const fs = require('fs');
const nodemailer = require('nodemailer');

const app = express()
app.use(cors());
app.use(bodyParser.json());
app.set('PORT', process.env.PORT || 7070);

var workbook = new Excel.Workbook()
var transporter = nodemailer.createTransport({
	service: 'Gmail',
	auth: {
	    user: 'mailtest4159@gmail.com',
	    pass: 'Linde123'
	}
});

app.post('/test', (req, res) => {
  	const payload = req.body.data;
  	var results = [];
	workbook.xlsx.readFile("./../database/25-1-2018.xlsx").then(function (){
		var worksheet=workbook.getWorksheet('Sheet1');
		worksheet.eachRow({includeEmpty: true}, function(row, rowNumber){
			if(rowNumber>=3 && row.values[2].toLowerCase()==payload.product && row.values[3]>=payload.purity && row.values[5]<=payload.period) {
				results.push({
					supplier: row.values[6],
					name: row.values[1],
					purity: row.values[3],
					leadTime: row.values[5],
					capex: row.values[4],
				})
			}
		})
	}).then(function (){
		res.send(results)
	})
});

app.post('/email', (req, res) => {
	//Generate PDF document first
	var doc = new PDFDocument();
	doc.pipe(fs.createWriteStream('./pdfs/file.pdf'))

	const cusInfo="Name: "+req.body.data.cusname+"\n\nCountry: "+req.body.data.country+"\n\nEmail: "+req.body.data.email+"\n\nAdditional Information: "+req.body.data.comments+"\n\n"
	const prodInfo="Product Name: "+req.body.data.name+"\n\nProduct: "+req.body.data.product+"\n\nPurity: "+req.body.data.purity+"\n\nTime Period: "+req.body.data.period+" Months\n\n"
	height = doc.currentLineHeight();
	// draw some text
	doc.fontSize(20)
	   .text('Sample Contract', 50, 80);
	   
	doc.fontSize(17).font('Courier')
	    .text('Customer Information', 50, 135)
	    .underline(50, doc.y, doc.widthOfString('Customer Information'), doc.currentLineHeight()-50)
	   .font('Times-Roman', 12.5)
	   .moveDown(1.5)
	   .text(cusInfo, {
	     width: 412,
	     align: 'justify',
	     columns: 2,
	     height: 300,
	     ellipsis: true
	   });

	doc.fontSize(17).font('Courier')
	    .text('Product Information', 50, 300)
	    .underline(50, doc.y, doc.widthOfString('Product Information'), doc.currentLineHeight()-50)
	    .font('Times-Roman', 12.5)
	    .moveDown(1.5)
	    .text(prodInfo, {
	     width: 412,
	     align: 'justify',
	     columns: 2,
	     height: 300,
	     ellipsis: true
	   });

	var date = new Date()
	var ddmmyy = date.getFullYear() + "/" + (date.getMonth()+1<10?0:"") + (date.getMonth()+1) + "/" + (date.getDate()<10?0:"") + (date.getDate())

	doc.fontSize(13).font('Helvetica')
	    .text(ddmmyy, 50, 530)
	    .underline(50, doc.y, 65, doc.currentLineHeight()-40)
	    .text('Date', 50, 550)

	doc.end();


	//Generate excel file for admin
	var capex = req.body.data.capex
	var period = req.body.data.period

	var adminExcel = new Excel.Workbook()
	adminExcel.creator = "Linde"
	adminExcel.lastModifiedBy = 'Linde'
	adminExcel.created = date
	adminExcel.modified = date
	adminExcel.lastPrinted = date

	var adminSheet = adminExcel.addWorksheet('Prices')
	adminSheet.columns = [
		{ header: 'Customer Name', key: 'cusname', width: 20},
		{ header: 'Country', key: 'country', width: 20},
		{ header: 'Email', key: 'email', width: 20},
		{ header: 'Comments', key: 'comments', width: 20},
		{ header: 'Product Name', key: 'prodname', width: 20},
		{ header: 'Product', key: 'product', width: 9},
		{ header: 'Purity(%)', key: 'purity', width: 9},
	    { header: 'Capex', key: 'capex', width: 20 },
	    { header: 'Time Period', key: 'period', width: 20 },
	    { header: 'Monthly Fee', key: 'monfee', width: 20 }
	];

	var cusnameCol = adminSheet.getColumn('cusname')
	var countryCol = adminSheet.getColumn('country')
	var emailCol = adminSheet.getColumn('email')
	var commentsCol = adminSheet.getColumn('comments')
	var prodnameCol = adminSheet.getColumn('prodname')
	var productCol = adminSheet.getColumn('product')
	var purityCol = adminSheet.getColumn('purity')
	var capexCol = adminSheet.getColumn('capex')
	var timeCol = adminSheet.getColumn('period')
	var monfeeCol = adminSheet.getColumn('monfee')

	cusnameCol.values = [,,req.body.data.cusname]
	countryCol.values = [,,req.body.data.country]
	emailCol.values = [,,req.body.data.email]
	commentsCol.values = [,,req.body.data.comments]
	prodnameCol.values = [,,req.body.data.name]
	productCol.values = [,,req.body.data.product]
	purityCol.values = [,,req.body.data.purity]
	capexCol.values = [,,capex]
	timeCol.values = [,,period]
	monfeeCol.values = [,,capex/period]

	adminExcel.xlsx.writeFile('./excels/admin.xlsx')



	//send pdf contract email
	//setup email data for customer, sales, bcc admin
  	var mailOptions = {
        from: "Linde Testing", // sender address
        to: 'mailtest4159@gmail.com, salestest4159@gmail.com', // list of receivers
        bcc: 'admtest4159@gmail.com',
        subject: 'Sample Contract', // Subject line
        text: 'Test', // plain text body
        html: '<b>Sample Contract attached as a PDF File.</b>', // html body
        attachments:[{
        	filename: 'file.pdf',
        	path: './pdfs/file.pdf'
        }]
    };

	// send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
    });

    mailOptions = {
    	from: "Linde Testing",
    	to: 'admtest4159@gmail.com',
    	subject: 'sample excel sheet',
    	text: 'Test', // plain text body
        html: '<b>Sample data attached as a excel File.</b>', // html body
        attachments:[{
        	filename: 'admin.xlsx',
        	path: './excels/admin.xlsx'
        }]
    }

    transporter.sendMail(mailOptions, (error, info) => {
    	if (error) {
    		return console.log(error);
    	}
    	console.log('Message sent: %s', info.messageId);
    })

    res.send("OK")
})



app.listen(app.get('PORT'), () => 
  console.log('Listening at ' + app.get('PORT')))